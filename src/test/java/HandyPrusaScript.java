import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import prusa.PrusaRobot;

public class HandyPrusaScript {


    private PrusaRobot p;

    @Before
    public void setUp() throws Exception {
        p = new PrusaRobot("COM11","COM22");
    }

    private void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStartOnly() {
        assert true;
    }

    @Test
    public void paymentChipcard() {
/*        p.typeAmount("125780");
        p.typeAmount("<");
        p.typeAmount("3469"); */
        p.typeAmount("1");
        p.typeAmount("00");
        p.amountOk();
        sleep(1000);
        p.chipCardIn();
        sleep(4000);
        p.selectAccount("Credit");
        sleep(2000);
        p.typePin("8224");
        p.amountOk();
         sleep(18000);
        //
        // p.waitForTxResponse();
        p.chipCardOut();
        sleep(2000);
        p.pressFinish();
        System.out.println("Transaction finished with TRC:"+p.getLastTrc()+" HRC:"+p.getLatHrc());
        p.die();
    }

    @Test
    public void paymentNfcCard() {
/*        p.typeAmount("125780");
        p.typeAmount("<");
        p.typeAmount("3469"); */
        p.typeAmount("1");
        p.typeAmount("00");
        p.amountOk();
        sleep(1000);
        p.nfcCardOn();
        sleep(2000);
        p.nfcCardOff();
        sleep(2000);
        p.selectAccount("Credit");
        sleep(2000);
        p.typePin("8226");
        p.amountOk();
//        sleep(18000);
        sleep(2000);
        p.waitForTxResponse();
        p.pressFinish();
        System.out.println("Transaction finished with TRC:"+p.getLastTrc()+" HRC:"+p.getLatHrc());
        p.die();
    }

    @After
    public void tearDown() throws Exception {
        p.die();
    }

}
