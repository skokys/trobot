import arm.ArmController;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static java.lang.Thread.sleep;

/**
 * script for servo hand
 */
@FixMethodOrder(MethodSorters.DEFAULT)
public class ArmScript {

	private static ArmController arm=null;

    @BeforeClass
	public static void setUp() throws Exception {
		if (arm==null) {
            arm = new ArmController("COM22");
            arm.handUp();
            arm.middle2();
        }
	}


	@Test
	public void testFingers() throws InterruptedException {
		arm.setFingerClose();
		sleep(2000);
		arm.setFingerOpen(100);
		sleep(2000);
		arm.setFingerOpen(50);
	}

	@Test
	public void testHandLeftRight() throws InterruptedException {
		arm.setHandLeft();
		sleep(1000);
		arm.setHandRight();
		sleep(1000);
		arm.setHandLR(0);
		sleep(1000);
		arm.setHandLR(100);
		sleep(1000);
		arm.setHandLR(50);
		sleep(2000);
	}

	@Test
	public void testHandUpDown() throws InterruptedException {
		arm.handUp();
		sleep(1000);
		arm.handDown();
		sleep(1000);
	}

    @Test
    public void testArmUpDown() throws InterruptedException {
        arm.up();
        sleep(2000);
        arm.down();
        sleep(2000);
    }

    @Test
    public void setArm2UpDown() throws InterruptedException {
        arm.up();
        arm.up2();
        sleep(1000);
        arm.down2();
        sleep(1000);
        arm.middle2();
    }

    @Test
    public void testBase() throws InterruptedException {
        arm.baseLeft();
        sleep(2000);
        arm.baseRight();
        sleep(2000);
    }

	@AfterClass
	public static void tearDown() throws Exception {
        arm.rest();
        sleep(1000);
        arm.close();
	}

}
