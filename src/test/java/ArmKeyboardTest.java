import static java.lang.Thread.sleep;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import arm.ArmController;

/**
 * script for servo hand
 */
@FixMethodOrder(MethodSorters.DEFAULT)
public class ArmKeyboardTest {

	private static ArmController arm=null;

    @BeforeClass
	public static void setUp() throws Exception {
		if (arm==null) {
            arm = new ArmController("COM22");
            arm.rest();
            sleep(1000);
        }
	}

    @Test
    public void testGetPosition() throws InterruptedException {
        arm.setPosition(2, 1400, 10, 1);
        sleep(3000);
        arm.setPosition(2, 1400, 10, 1);
        sleep(3000);
    }


    @Test
    public void testBase() throws InterruptedException {
        arm.baseLeft();
        sleep(2000);
        arm.baseRight();
        sleep(2000);
        arm.baseMiddle();
        sleep(2000);
    }

    @Test
    public void testPress5V2() throws InterruptedException {
        // 5
        arm.setPosition(2,2032,0,0);
        arm.setPosition(3,1837);
        arm.setPosition(4,1486,15,3);
        arm.setPosition(5,2048);
        sleep(3000);
        arm.setPosition(4,1460,5,1);
        sleep(500);
        arm.setPosition(4,1510,5,1);
        sleep(1000);

        // 00
        arm.setPosition(5,2166);
        arm.setPosition(2,2016,20,5);
        arm.setPosition(3,1850);
        arm.setPosition(4,1496,15,3);
        sleep(3000);
        arm.setPosition(4,1440,2,1);
        sleep(1000);
        arm.setPosition(4,1496,2,1);
        sleep(1000);

        // OK
        arm.setPosition(2,2122,20,10);
        arm.setPosition(3,1729);
        arm.setPosition(4,1378,15,3);
        arm.setPosition(5,2129);
        sleep(2000);
        arm.setPosition(2,1960,0,0);
        sleep(1000);
        arm.rest();
        sleep(1000);

        arm.setPosition(2,937,0,0);
        arm.setPosition(3,1736);
        arm.setPosition(4,1525,15,3);
        arm.setPosition(5,1588);
        sleep(2000);

    }


    @Test
    public void testPress5() throws InterruptedException {
        // 5
        arm.setPosition(2,2295,0,0);
        arm.setPosition(3,1860);
        arm.setPosition(4,1478,15,3);
        arm.setPosition(5,2025);
        sleep(2000);
        arm.setPosition(2,2150,50,10);
        sleep(500);
        arm.setPosition(2,2295,50,10);
        sleep(300);

        // 00
        arm.setPosition(2,2118,20,5);
        arm.setPosition(3,1824);
        arm.setPosition(4,1461,15,3);
        arm.setPosition(5,2137);
        sleep(2000);
        arm.setPosition(2,1980,20,10);
        sleep(300);
        arm.setPosition(2,2118,20,10);
        sleep(200);

        // OK
        arm.setPosition(2,2122,20,10);
        arm.setPosition(3,1729);
        arm.setPosition(4,1378,15,3);
        arm.setPosition(5,2129);
        sleep(2000);
        arm.setPosition(2,1960,0,0);
        sleep(300);

    }



    @Test
    public void testPressFromBack() throws InterruptedException {
        // 5
        arm.setPosition(2, 1884,0,0);
        arm.setPosition(3, 1510,20,10);
        arm.setPosition(4, 1290,20,10);
        arm.setPosition(5, 1680,20,10);
        sleep(2000);
       // arm.setPosition(2, 1750,0,0);
        arm.setPosition(3, 1570,0,0);
        sleep(1000);
       // arm.setPosition(2, 1884,10,10);
        arm.setPosition(3, 1499,10,10);
        sleep(2000);

        // 00
        arm.setPosition(2, 1880,0,0);
        arm.setPosition(3, 1371);
        arm.setPosition(4, 1192,20,10);
        arm.setPosition(5, 1707,20,10);
        sleep(2000);
        // arm.setPosition(2, 1859,0,0);
        arm.setPosition(3, 1455,0,0);
        sleep(1000);
        // arm.setPosition(2, 1880,10,10);
        arm.setPosition(3, 1371,10,10);
        sleep(2000);


        // 00
        arm.setPosition(2, 1867,0,0);
        arm.setPosition(3, 1216);
        arm.setPosition(4, 1094);
        arm.setPosition(5, 1661);
        sleep(2000);
     //   arm.setPosition(2, 1859);
        arm.setPosition(3, 1270,0,0);
        sleep(1000);
//        arm.setPosition(2, 1880);
//        arm.setPosition(3, 1371);
//        sleep(2000);

    }



    @Test
    public void testNfc() throws InterruptedException {
        arm.setPosition(2,937,0,0);
        arm.setPosition(3,1736);
        arm.setPosition(4,1525,15,3);
        arm.setPosition(5,1588);
        sleep(2000);

    }


    @Test
    public void testArm() throws InterruptedException {
        arm.handMiddle();
        sleep(2000);
        arm.handUp();
        sleep(2000);
        arm.handDown();
        sleep(2000);
        arm.handMiddle();
        sleep(2000);
    }


    @AfterClass
	public static void tearDown() throws Exception {
        arm.rest();
        arm.close();
	}



}
