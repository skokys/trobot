import javax.swing.*;
import java.util.List;

/**
 * Created by ladislav.skokan on 14.3.14.
 */
public class RobotPage extends JFrame {
    private JList testList;
    private JButton connectPrusa;
    private JTextField adbConnStatus;
    private JTextField raspiPortText;
    private JTextField prusaPortText;
    private JPanel rootPanel;
    private JButton runAll;
    private DefaultListModel<String> model;

    public RobotPage() {
        super("Robot");
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 600);
        setContentPane(rootPanel);
        setVisible(true);

    }

    public String getRaspiUrl() {
        return raspiPortText.getText();
    }

    public String getPrusaPort() {
        return prusaPortText.getText();
    }

    public void updateTests(List<String> tests) {
        model = new DefaultListModel<String>();
        for (String t : tests) {
            model.addElement(t);
        }
        testList.setModel(model);
    }
}
