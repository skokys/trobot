package adb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ladislav.skokan on 7.3.14.
 */
public class AdbViewer {

    private String[] positions;
    private int last = 0;

    private String lastTRC;
    private String lastHRC;
    public boolean adbConnected = false;
    private Process p;
    private BufferedReader reader;
    private BufferedReader readerErrors;

    public AdbViewer(boolean local) throws InterruptedException, IOException {
        while (true) {
            init();
            p.waitFor();
            System.out.println("ADB exited. Exit code:" + p.exitValue());
            adbConnected=false;
        }

    }

    private void init() throws IOException {
        p = Runtime.getRuntime().exec("adb logcat");

        reader = new java.io.BufferedReader(new InputStreamReader(p.getInputStream()));
        new LogcatWatcher(reader).start();
        readerErrors = new java.io.BufferedReader(new InputStreamReader(p.getErrorStream()));
        new LogcatErrorWatcher(readerErrors).start();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        new AdbViewer(true);
    }

    public AdbViewer() throws IOException {
        init();
    }

    public void clearTxResulats() {
        lastTRC = null;
        lastHRC = null;

    }

    private class LogcatWatcher extends Thread {


        private final BufferedReader reader;

        public LogcatWatcher(BufferedReader reader) {
            setName("Output reader");
            this.reader = reader;
        }

        @Override
        public void run() {
            System.out.println("Starting ADB watchdog....");

            try {
                String s;
                while ((s = reader.readLine()) != null) {
                    adbConnected = true;
                    //    System.out.println("Log:"+s);
                    if (s.contains("Initializing pin process")) {
                        clearPositions();
                    } else if (s.contains("waiting for device")) {
                        throw new IllegalStateException("ADB not connected!");
                    } else if (s.contains("Creating coordinates for keyType:")) {
                        System.out.println("Pinpad positions received");
                        s = s.replaceAll(".*Creating coordinates for keyType:", "");
                        System.out.println(s);
                        if (last < 16) {
                            String[] fields = s.split(",");
                            if (positions[last] == null) positions[last] = "";
                            for (int i = 1; i < fields.length - 1; i++) {
                                String[] f = fields[i].split(":");
                                positions[last] = positions[last] + f[1] + ";";
                            }
                            last++;
                        } else if (last == 16) System.out.println("Pinpad keys positions set");
//                    } else if (s.contains("TXDescriptor") && s.contains("TRC")) {
//                        // TXDescriptor﹕ Change TRC -> Old value = X0_NO_RESPONSE, new value = TF_LOGON_REQUIRED
//                        String[] f = s.split("=");
//                        System.out.println("TRC change:"+f[2]);
//                        lastTRC =f[2].substring(0,2);
//                    } else if (s.contains("TXDescriptor") && s.contains("HRC")) {
//                        String[] f = s.split("=");
//                        System.out.println("HRC change:"+f[2]);
//                        lastHRC =f[2].substring(0,2);
                    } else if (s.contains("W/TRANSACTION") && s.contains("{BUSINESS} Transaction [")) {
                        System.out.println("Transaction finished");
                        // [id=54, amountTotal=7, txType=PURCHASE, status=COMPLETED, transactionStatus=DECLINED,
                        // HRC=null, TRC=E2_EMV_CARD_REMOVED, STAN=0, accountType=CHEQUE_DEBIT, cardEntryMode=CHIP,
                        // cardType=DEBIT, offline=false, efbSaf=false, dateAndTime=Sun Mar 09 03:40:34 AEDT 2014,
                        // cardNumberMasked=0316, terminalId=32529052, amount=7, amountOther=0, amountTip=0,
                        // applicationLabel=MasterCard, cardName=MASTERCARD, expirationDateMasked=, signaturePrinted=false,
                        // appMode=STANDALONE, emvType=PARTIAL]
                        String[] f = s.split(",");
                        lastHRC = f[5];
                        lastTRC = f[6];
                        System.out.println("TRC:"+lastTRC+" HRC:"+lastHRC);

//                    } else if (s.startsWith("D/")) {
//                        System.out.println(s);
                    } else if (s.contains("Payment result:")) {
                        String[] f = s.split(":");
                        String[] ff = f[2].split(",");
                        lastHRC = ff[0];
                        lastTRC = ff[1];
                        System.out.println("TRC:"+lastTRC+" HRC:"+lastHRC);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void clearPositions() {
        positions = new String[20];
        last = 0;
    }

    public boolean positionsReady() {
        if (last == 16) return true;
        return false;
    }

    public String[] getPositions() {
        return positions;
    }

    public String getLastTRC() {
        return lastTRC;
    }

    public String getLastHRC() {
        return lastHRC;
    }

    private class LogcatErrorWatcher extends Thread {
        private final BufferedReader readerErrors;

        public LogcatErrorWatcher(BufferedReader readerErrors) {
            setName("Errors reader");
            this.readerErrors = readerErrors;
        }

        @Override
        public void run() {
            super.run();
            String s;
            try {
                // System.out.println("Starting error log");
                while ((s = readerErrors.readLine()) != null) {
                    System.out.println("ADB ERR:" + s);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
