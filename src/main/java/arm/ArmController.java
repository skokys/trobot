package arm;

import sun.plugin.dom.exception.InvalidStateException;

import java.io.*;
import java.nio.ByteBuffer;

import static java.lang.Thread.sleep;

/**
 * Created by ladislav.skokan on 16.5.2014.
 */
public class ArmController {

    private static final int SPEED = 40;
    private static final int ACCELERATION = 20;
    private int POS_MID = 1500;
    private int POS_MAX = 2300;
    private int POS_MIN = 600;

    private final PololuSerialPort port;

	public ArmController(String portS) throws Exception {
		port = new PololuSerialPort(portS);
		init();
	}

	private void init() {
		for (int i = 0; i < 6; i++) {
			setSpeed(i, SPEED);
			setAcceleration(i, 20);
		}
	}

	private void setAcceleration(int servo, int acc) {
		try {
			port.out.write(new byte[] { Command.SET_ACCELERATION, (byte) servo, (byte) acc, 0 });
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void setSpeed(int servo, int speed) {
		try {
			port.out.write(new byte[] { Command.SET_SPEED, (byte) servo, (byte) speed, 0 });
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param percent
	 *            of open fingers
	 */
	public void setFingerOpen(int percent) {
		setPosition(0, 0x7035 - percent / 3, 100, 10);

	}

	public void setHandLeft() {
		setPosition(1, POS_MAX, 100, 10);
	}

	public void setHandRight() {
		setPosition(1, POS_MIN, 100, 10);
	}

	public void setHandLR(int percent) {
		setPosition(1, (int) (POS_MAX - percent / (POS_MAX / POS_MIN)), 100, 10);

	}

	public void handUp() {
		setPosition(2, POS_MIN , SPEED, ACCELERATION);
	}

	public void handDown() {
		setPosition(2, POS_MAX, 100, 10);
	}

	public void up() {
		setPosition(3, POS_MIN -20, 100, 10);
	}

	public void down() {
		setPosition(3, POS_MAX, 100, 10);
	}

    public void middle() {
        setPosition(3, 1412, 100, 10);
    }

    public void up2() {
		setPosition(4, POS_MAX, 50, 5);
	}

	public void down2() {
		setPosition(4, POS_MIN, 50, 5);
	}


	public void middle2() {
		setPosition(4, 1800, SPEED, ACCELERATION);

	}

	public void baseLeft() {
		setPosition(5, POS_MIN, SPEED, ACCELERATION);
	}

	public void baseRight() {
        setPosition(5, POS_MAX, SPEED, ACCELERATION);
    }

	public void close() {
        try {
            port.out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void baseMiddle() {
        setPosition(5, POS_MID);

    }


	public void setPosition(int servo, int pos, int speed, int acc) {
		setSpeed(servo, speed);
		setAcceleration(servo, acc);
		setPositionOnly(servo, pos);
	}

	public void setPosition(int servo, int p) {
		setSpeed(servo, SPEED);
		setAcceleration(servo, ACCELERATION);
		setPositionOnly(servo, p);
	}

    private void setPositionOnly(int servo, int pos) {
        logPosition(servo);
        int p = pos * 8;
        if (pos<POS_MIN || pos>POS_MAX)
            throw new InvalidStateException("Wrong position "+servo+"/"+p);

        ByteBuffer bb = ByteBuffer.allocate(4);
		bb.putInt(p);
        byte low = (byte) (bb.array()[3] & 0x7f);
        byte high = (byte) (bb.array()[2] & 0x7f);

		byte[] bbb = new byte[] { Command.SET_POSITION, (byte) servo, low, high };
		try {
			port.out.flush();
            port.out.write(bbb);
            port.out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

    private void logPosition(int servo) {
        byte[] bbb = new byte[] { Command.GET_POSITION, (byte) servo};
        try {
            port.out.flush();
            port.out.write(bbb);
            port.out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setFingerClose() {
		setPosition(0, 0x7035, 100, 10);

	}

    public void handMiddle() {
        setPosition(2, POS_MID , SPEED, ACCELERATION);
    }

    public void rest() {
        handUp();
        middle2();
        middle();
    }

    public void fingerUp() {
        setPosition(6,820,0,5);
    }

    public void fingerDown(int i) {
        setPosition(6,1470+i*10,0,5);
    }
}
