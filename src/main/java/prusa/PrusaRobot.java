package prusa;

import arm.ArmController;

import java.io.IOException;

import static junit.framework.Assert.assertFalse;

/**
 * Created by ladislav.skokan on 2.2.14.
 */
public class PrusaRobot {

    public static final String WAIT_FOR_BUFFER_CLEAN = "M400\n";
    private static final String SET_ACCELERATION = "M204 S8000\n";
    public static final String GO_HOME = "G28\nG92 E0\n";
    private static final String SET_MM = "G21\n";
    private static final String GET_INFO = "M241\n";
    private static final String FAN_OFF = "M106 S0\n";

    private PrusaOverUsbHandler prusa;
    private ArmController pololu;

    public PrusaRobot(String portPrusa) {
        connectPrusaOverUsb(portPrusa);
    }

    public PrusaRobot(String portPrusa, String portPololu) {
        connectPrusaOverUsb(portPrusa);
        connectPololu(portPololu);
    }

    private void connectPrusaOverUsb(String port) {
        try {
            prusa = new PrusaOverUsbHandler(port, new PositionVerifier(), getInitStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void connectPololu(String port) {
        try {
            pololu = new ArmController(port);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


//    private boolean startAdbWatchdog() {
//        try {
//            // adbView = new AdbViewer();
//            return true;
//        } catch (IOException e) {
//            return true;
//        }
//    }

    public static byte[] getInitStream() {
        System.out.println("Starting init");
        StringBuffer bb = new StringBuffer();
        bb.append(PrusaRobot.GO_HOME);
        bb.append(PrusaRobot.SET_ACCELERATION);
        bb.append(PrusaRobot.SET_MM);      // mms
        bb.append(PrusaRobot.GET_INFO);     // info
        bb.append(PrusaRobot.FAN_OFF);
        bb.append(WAIT_FOR_BUFFER_CLEAN);     // wait for buffer clean;
        return bb.toString().getBytes();
    }

    public void amountOk() {
        setFingerUp();
        prusa.setPosition(145, 35);
        setFingerDown(35);
        setFingerUp();
    }

    int[] posX = new int[]{145, 115, 145, 175, 115, 145, 175, 115, 145, 175};
    int[] posY = new int[]{55, 105, 105, 105, 85, 85, 85, 70, 70, 70};

    public void typeAmount(String s) {
        if (prusa ==null) assertFalse("Prusa nt connected", true);
        setFingerUp();
        if (s == null) return;
        if (s.equals("00")) {
            prusa.setPosition(115, 55);
            setFingerDown(20);
            setFingerUp();
        } else if (s.equals("<")) {
            prusa.setPosition(175, 55);
            setFingerDown(20);
            setFingerUp();
        } else
            for (int i = 0; i < s.length(); i++) {
                String k = s.substring(i, i + 1);
                Integer num = Integer.valueOf(k);
                prusa.setPosition(posX[num], posY[num]);
                setFingerDown((105 - posY[num]) / 3 + 8);
                setFingerUp();
            }

    }

    private void setFingerUp() {
       pololu.fingerUp();
        sleep(1000);
    }

    private void setFingerDown(int i) {
       pololu.fingerDown(i);
        sleep(1000);
    }

    public void chipCardIn() {

    }

    public void chipCardOut() {

    }


    public void nfcCardOn() {

    }

    public void nfcCardOff() {

    }



    public void selectAccount(String account) {
        // adbView.clearPositions();
        setFingerUp();
        if (account.equals("Credit")) {
            prusa.setPosition(145, 100);
            setFingerDown(10);
            setFingerUp();
        } else if (account.equals("Debit")) {
            prusa.setPosition(145, 70);
            setFingerDown(12);
        } else
            System.out.println("ERROR! Invalid account type");
    }

    public void typePin(String pin) {
        if (pin == null) return;
        setFingerUp();
        String ppos[] = null; // waitForPinPositionsInPixels();

        if (ppos == null) throw new IllegalStateException("PINPAD positions not received");

        for (int i = 0; i < pin.length(); i++) {   //TODO doplnit clear, <, OK, cancel
            String k = pin.substring(i, i + 1);
            Integer num = Integer.valueOf(k);
            String[] pos = ppos[num].split(";");
            Integer px = Integer.valueOf(pos[0]);
            Integer py = Integer.valueOf(pos[1]);
            prusa.setPositionPx(px + 50, py - 50);
            setFingerDown((105 - posY[num]) / 3 + 8);
            setFingerUp();
        }


        prusa.setPositionPx(800, 1200);

    }

//    private String[] waitForPinPositionsInPixels() {
////        adbView.clearPositions();
//        for (int i = 0; i < 100; i++) {
//            if (adbView.positionsReady())
//                return adbView.getPositions();
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }

    public void pressPrint() {
        setFingerUp();
        setPosition(115, 35);
        setFingerDown(35);
        setFingerUp();
    }

    private void setPosition(int x, int y) {

    }

    public String getLatHrc() {
        // return adbView.getLastHRC();
        return "00";
    }

    public String getLastTrc() {
        // return adbView.getLastTRC();
        return "00";

    }

    public void pressFinish() {
        amountOk();
    }

    public void waitForTxResponse() {
//        adbView.clearTxResulats();
//
//        for (int i = 0; i < 200; i++) {
//            if (adbView.getLastHRC() != null || adbView.getLastTRC() != null) {
//                return;
//            } else
//                sleep(200);
//        }

    }


    public class PositionVerifier {
        public boolean positionSet = false;

        public void waitForPositionSet() throws IOException {
            // sleeps 5 secs or until position is set to preset
            for (int i = 0; i < 100; i++) {
                prusa.write("M114\n".getBytes());
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (positionSet) return;   // serial reader sets the position
            }
            throw new IllegalStateException("Position not set timeout");
        }


        /**
         * checks if position is set from this line ---
         * X:110.00Y:185.00Z:7.00E:0.00 Count X: 0.00Y:0.00Z:0.01
         * ---
         *
         * @param line
         */
        public void parsePos(String line) {
 //           System.out.println("D:"+line);
            String[] s = line.split(":");
            if (s.length < 5) return;

            float x, y, z, a, b, c;

            a = Float.valueOf(s[1].substring(0, s[1].length() - 1));
            b = Float.valueOf(s[2].substring(0, s[2].length() - 1));
            c = Float.valueOf(s[3].substring(0, s[3].length() - 1));


            x = Float.valueOf(s[5].substring(0, s[5].length() - 1));
            y = Float.valueOf(s[6].substring(0, s[6].length() - 1));
            z = Float.valueOf(s[7].substring(0, s[7].length() - 1));

            if (Math.round(x) == Math.round(a) &&
                    Math.round(y) == Math.round(b) &&
                    Math.round(z) == Math.round(c)) {
                positionSet = true;
                System.out.println("Position set true");
            } else positionSet = false;
        }

    }

    private void sleep(int i) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void die() {
            setFingerUp();
            chipCardOut();
            nfcCardOff();

        if (prusa !=null) {
            prusa.setPosition(0, 0);
            prusa.die();
        }

    }


}
