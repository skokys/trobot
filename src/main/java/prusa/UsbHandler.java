package prusa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Enumeration;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import sun.plugin.dom.exception.InvalidStateException;

import static java.lang.Thread.sleep;

/**
 * Created by ladislav.skokan on 2.2.14.
 */
public class UsbHandler {

	private final PrusaRobot.PositionVerifier posVer;
	public OutputStream out;
	private InputStream in;

	public UsbHandler(String port, PrusaRobot.PositionVerifier positionVerifier, byte[] initStream) throws Exception {
		this.posVer = positionVerifier;
		connect(port);
        sleep(1000);
		out.write(initStream);
		out.flush();
		sleep(2000);
	}

	private void connect(String portName) throws Exception {
		// Enumeration e = CommPortIdentifier.getPortIdentifiers();
		// CommPortIdentifier port = (CommPortIdentifier) e.nextElement();
		// System.out.println("Port:"+port.getName());
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
		if (portIdentifier.isCurrentlyOwned()) {
			System.out.println("Error: Prusa port is currently in use");
			throw new InvalidStateException("COM port in use");
		} else {
			int timeout = 2000;
			System.out.println("Opening port");
			CommPort commPort = portIdentifier.open(this.getClass().getName(), timeout);

			if (commPort instanceof gnu.io.SerialPort) {
				gnu.io.SerialPort serialPort = (gnu.io.SerialPort) commPort;
				serialPort.setSerialPortParams(115200, gnu.io.SerialPort.DATABITS_8, gnu.io.SerialPort.STOPBITS_1,
						gnu.io.SerialPort.PARITY_NONE);

				in = serialPort.getInputStream();
				(new Thread(new SerialReader(in))).start();
				out = serialPort.getOutputStream();
			} else {
				throw new IllegalArgumentException("Invalid use");
			}
		}
	}

	public void die() {
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	float[] pxMin = new float[] { 115, 195 };
	float[] pxMax = new float[] { 188, 30 };

	public void setPositionPx(int xPx, int yPx) {
		float xStep = (pxMax[0] - pxMin[0]) / 800;
		float posX = (xPx * xStep) + pxMin[0];

		float yStep = (pxMin[1] - pxMax[1]) / 1200;
		float posY = 180 - yStep * yPx;

		setPosition(posX, posY);
	}

	public class SerialReader implements Runnable {

		InputStream in;

		public SerialReader(InputStream in) {
			this.in = in;
		}

		public void run() {
			Thread.currentThread().setName("Reader");
			System.out.println("Starting reader thread [" + Thread.currentThread().getName() + "]");
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			while (true) {

				try {
					String line = br.readLine();
					System.out.println(">" + line);
					if (line.contains("Count X:") && line.contains("X:"))
						posVer.parsePos(line);
				} catch (IOException e) {
					// System.out.println("E:"+e.getMessage());
				}

			}
		}
	}

	protected void setPosition(float x, float y) {

		// calculate Z from 70 - 90 - based on position XY
		int xDiff = 200 - 60;
		float up = 0 + (18 / xDiff * (y - 60)); // should be 73
		up = Math.round(up);

		String cmdZ = "G1 Z" + (int) up + "\n";
		String cmdXY = "G1 X" + (int) x + " Y" + (int) y + " F8000\n";
		try {
			System.out.println("Setting pos:" + x + "/" + y + "/" + up);
			posVer.positionSet = false;
			out.write(cmdZ.getBytes());
			out.write(cmdXY.getBytes());
			posVer.waitForPositionSet();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
