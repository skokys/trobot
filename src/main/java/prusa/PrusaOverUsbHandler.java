package prusa;
import java.io.IOException;

public class PrusaOverUsbHandler {

    private final PrusaRobot.PositionVerifier posVer;
    private UsbHandler usb;

    public PrusaOverUsbHandler(String port,  PrusaRobot.PositionVerifier posVer, byte[] initStream) {
        this.posVer =  posVer;
        try {
            usb = new UsbHandler(port,posVer,initStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("Raspi not connected!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void die() {

        if (usb != null) {
            try {
                usb.out.write("M112".getBytes());
                usb.out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            usb.die();
        }
    }

    public void setPosition(float x, float y) {

        y+=10;
        // calculate Z from 70 - 90 - based on position XY
        int xDiff = 200 - 60;
        float up = 0 + (18 / xDiff * (y - 60));  // should be 73
        up = Math.round(up);

        String cmdZ = "G1 Z" + (int) up + "\n";
        String cmdXY = "G1 X" + (int) x + " Y" + (int) y + " F8000\n";
        try {
            System.out.println("Setting pos:" + x + "/" + y + "/" + up);
            posVer.positionSet = false;
            usb.out.write(cmdZ.getBytes());
            usb.out.write(cmdXY.getBytes());
            usb.out.flush();
            posVer.waitForPositionSet();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    float[] pxMin = new float[]{115,195};
    float[] pxMax = new float[]{188,30};

    public void setPositionPx(int xPx, int yPx) {
        float xStep = (pxMax[0] - pxMin[0]) / 800;
        float posX = (xPx * xStep) + pxMin[0];

        float yStep = (pxMin[1]-pxMax[1])/1200;
        float posY = 180-yStep*yPx;

        setPosition(posX,posY);
    }

    public void write(byte[] b) {
        try {
            usb.out.write(b);
            usb.out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
